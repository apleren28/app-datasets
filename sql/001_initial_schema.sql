-- +goose Up
CREATE TABLE IF NOT EXISTS CATEGORIES(
    id INT PRIMARY KEY,
    name VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS APPS(
    id SERIAL PRIMARY KEY,
    app_identifier VARCHAR(150) NOT NULL,
    category INT NOT NULL,
    size INT,
    release_date DATE,
    average_rating NUMERIC,
    reviews INT,
    platform VARCHAR(10)
);

CREATE INDEX idx_app_category
ON APPS(category);

CREATE INDEX idx_app_release_date
ON APPS(release_date);

INSERT INTO CATEGORIES (id, name) 
VALUES
    (1, 'GAME'),
    (2, 'MUSIC'),
    (3, 'HEALTH');

-- +goose Down
DROP TABLE CATEGORIES;
DROP TABLE APPS;