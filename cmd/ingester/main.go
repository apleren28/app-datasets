package main

import (
	"fmt"
	"log"
	"os"

	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/service/apparser"
	"gitlab.com/apleren28/internal/service/filereader"
	"gitlab.com/apleren28/internal/storage/postgres"
)

func main() {
	envFile := os.Getenv("ENV_FILE")
	godotenv.Load(envFile)

	cfg := domain.EnvConfig{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalln("error parsing env vars", err)
	}

	dbInstance, err := postgres.NewPGBD(
		cfg.DBUser,
		cfg.DBPassword,
		cfg.DBName,
		cfg.DBPort,
		cfg.DBMaxIdleConn,
		cfg.DBMaxOpenCoon,
	)
	if err != nil {
		panic(err)
	}

	androidNotifier := make(chan int64)
	iOSNotifier := make(chan int64)
	defer close(androidNotifier)
	defer close(iOSNotifier)

	go func(androidNotifier chan int64) {
		for n := range androidNotifier {
			fmt.Println("have ingested other", n, "Android records")
		}

	}(androidNotifier)

	go func(iOSNotifier chan int64) {
		for n := range iOSNotifier {
			fmt.Println("have ingested other", n, "iOS records")
		}

	}(iOSNotifier)

	androidParser := apparser.NewAndroidApp(
		filereader.NewCSVFileReader(true),
		dbInstance,
		androidNotifier,
	)

	iOSParser := apparser.NewIOSApp(
		filereader.NewCSVFileReader(true),
		dbInstance,
		iOSNotifier,
	)

	if err := androidParser.ParseAndStoreAppData(fmt.Sprintf("data/%s/androidAppData.csv", cfg.Environment)); err != nil {
		log.Fatalf("error parsing android dataset")
	}

	if err := iOSParser.ParseAndStoreAppData(fmt.Sprintf("data/%s/iosAppData.csv", cfg.Environment)); err != nil {
		log.Fatalf("error parsing iOS dataset")
	}

}
