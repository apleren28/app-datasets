package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
	"github.com/pressly/goose/v3"
	"gitlab.com/apleren28/internal/domain"

	_ "github.com/lib/pq"
)

var migrationAction = flag.String("action", "invalid", "runs migration up or down")

// var envFile = flag.String("envFile", ".env.dist", "indicates the env file to be used")

func main() {
	flag.Parse()
	envFile := os.Getenv("ENV_FILE")
	godotenv.Load(envFile)

	cfg := domain.EnvConfig{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalln("error parsing env vars", err)
	}

	connStr := fmt.Sprintf("host=localhost port=%s user=%s password=%s dbname=%s sslmode=disable", cfg.DBPort, cfg.DBUser, cfg.DBPassword, cfg.DBName)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		panic(err)
	}

	if err := goose.SetDialect("postgres"); err != nil {
		panic(err)
	}

	if strings.ToLower(*migrationAction) == "up" {
		if err := goose.Up(db, "./sql"); err != nil {
			log.Fatalln("error creating DB strucutres", err)
		}

		return
	}

	if strings.ToLower(*migrationAction) == "down" {
		if err := goose.Down(db, "./sql"); err != nil {
			log.Fatalln("error destroying DB strucutres", err)
		}

		return
	}

	log.Fatal("invalid migration action")
}
