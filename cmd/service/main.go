package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/joho/godotenv"
	"gitlab.com/apleren28/internal/api"
	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/service/reports"
	"gitlab.com/apleren28/internal/storage/postgres"
)

func main() {

	envFile := os.Getenv("ENV_FILE")
	godotenv.Load(envFile)

	cfg := domain.EnvConfig{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalln("error parsing env vars", err)
	}

	dbInstance, err := postgres.NewPGBD(
		cfg.DBUser,
		cfg.DBPassword,
		cfg.DBName,
		cfg.DBPort,
		cfg.DBMaxIdleConn,
		cfg.DBMaxOpenCoon,
	)
	if err != nil {
		panic(err)
	}

	reportsHandler := api.NewReportsHandler(
		reports.NewReportService(
			dbInstance,
		),
	)

	// boopstrap http server
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(20 * time.Second))

	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods: []string{"GET", "OPTIONS"},
	}))

	r.Route("/api/v1", func(r chi.Router) {
		r.Get("/apps-by-size", reportsHandler.ListAppSizeByCategory)
		r.Get("/apps-avg-rating", reportsHandler.ListAppAvgRatingByCategory)
		r.Get("/apps-release-timeline", reportsHandler.ListReleasesOverTimeByCategory)
		r.Get("/apps-top-10-size", reportsHandler.ListTop10BiggestAppsByCategory)

	})

	fmt.Println("about to start http server on port 9090")
	http.ListenAndServe(":9090", r)
}
