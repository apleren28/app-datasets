package sql__test

import (
	"context"
	"log"
	"testing"

	"github.com/caarlos0/env/v6"
	"github.com/stretchr/testify/assert"
	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/storage"
	"gitlab.com/apleren28/internal/storage/postgres"
)

var dbInstance storage.Service

func TestGetReleasesOverTimeByCategory(t *testing.T) {
	db := GetDBConnection()
	result, err := db.GetReleasesOverTimeByCategory(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 4, len(result))
}

func TestGetAppAvgReviewByCategory(t *testing.T) {
	db := GetDBConnection()
	result, err := db.GetAppAvgReviewByCategory(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 2, len(result))
}

func TestGetAppSizeByCategory(t *testing.T) {
	db := GetDBConnection()
	result, err := db.GetAppSizesByCategory(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 3, len(result))
}

func TestGetTOp10AppsBySize(t *testing.T) {
	db := GetDBConnection()
	result, err := db.GetTop10BiggestApps(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 4, len(result))
}

func GetDBConnection() storage.Service {
	var err error
	cfg := domain.EnvConfig{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalln("error parsing env vars", err)
	}

	if dbInstance == nil {
		dbInstance, err = postgres.NewPGBD(
			cfg.DBUser,
			cfg.DBPassword,
			cfg.DBName,
			cfg.DBPort,
			cfg.DBMaxIdleConn,
			cfg.DBMaxOpenCoon,
		)
		if err != nil {
			panic(err)
		}
	}

	return dbInstance
}
