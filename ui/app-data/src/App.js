import "./App.css";
import AppsBySize from "./components/AppsBySize";
import AppAvgRating from "./components/AppsAvgRating";
import AppsTop10BySize from "./components/AppsTop10BySize";
import AppsReleaseTimeline from "./components/AppsReleaseTimeline";

function App() {
  return (
    <div className="App">
      <p> App size distribution grouped by category</p>
      <AppsBySize />
      <p> App average rating grouped by category</p>
      <AppAvgRating />
      <p> Release timeline grouped by category</p>
      <AppsReleaseTimeline/>
      <p> Top 10 apps by size grouped by category</p>
      <AppsTop10BySize/>
    </div>
  );
}

export default App;
