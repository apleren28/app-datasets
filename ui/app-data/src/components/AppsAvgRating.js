import React, { Component } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { getRandomRGBColor } from "../utils/randomRGB";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: true,
      text: "App average rating by category",
    },
  },
};

export const categories = ["Games", "Music", "Helath"];

class AppAvgRating extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reviews: [],
    };
  }

  componentDidMount() {
    const url = "http://localhost:9090/api/v1/apps-avg-rating";
    fetch(url)
      .then((response) => {
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((json) => {
        const data = []
        for (let i = 0; i < categories.length; i++) {
            let filteredAvgByCategory = json.find((r) => r.category == i + 1);
            data.push({ label: categories[i], data: [Number((filteredAvgByCategory.averge_review).toFixed(2))],  backgroundColor: getRandomRGBColor() });
        }
        this.setState({ reviews: data });
      })
      .catch((error) => {
        console.log("error: " + error);
      });
  }

  render() {
    const {reviews} = this.state;
    const finalData = {
        labels: ['rating'],
        datasets: reviews,
    };

    return  <Bar options={options} data={finalData} />;
  }
}

export default AppAvgRating;
