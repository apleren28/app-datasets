import React, { Component } from "react";
import { Box } from "@mui/material";
import MaterialReactTable from "material-react-table";

export const columns = [
  {
    header: "Category",
    accessorKey: "category",
    GroupedCell: ({ cell, row }) => (
      <Box sx={{ color: "primary.main" }}>
        <strong>{cell.getValue()} </strong>
      </Box>
    ),
  },
  {
    header: "Year",
    accessorKey: "year",
    enableGrouping: true,
  },
  {
    header: "Month",
    accessorKey: "month",
    enableGrouping: false,
  },
  {
    header: "Releases",
    accessorKey: "releases",
    enableGrouping: false,
  },
];

export const categories = ["Games", "Music", "Helath"];
const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

class AppsReleaseTimeline extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const url = "http://localhost:9090/api/v1/apps-release-timeline";
    fetch(url)
      .then((response) => {
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((json) => {
        json.forEach((item, i) => {
          json[i].category = categories[item.category - 1];
          json[i].year = Number(item.date.substr(0, 4));
          const dateParsed = new Date(item.date);
          json[i].date = dateParsed;
          json[i].month = monthNames[dateParsed.getUTCMonth()];
        });
        this.setState({ data: json });
      })
      .catch((error) => {
        console.log("error: " + error);
      });
  }

  render() {
    const { data } = this.state;
    return (
      <MaterialReactTable
        columns={columns}
        data={data}
        enableGrouping
        enablePagination={false}
        enableStickyHeader
        enableStickyFooter
        initialState={{
          density: "compact",
          expanded: false,
          grouping: ["category", "year"],
          sorting: [{ id: "year", desc: false }],
        }}
        muiToolbarAlertBannerChipProps={{ color: "primary" }}
        muiTableContainerProps={{ sx: { maxHeight: 700 } }}
      />
    );
  }
}

export default AppsReleaseTimeline;
