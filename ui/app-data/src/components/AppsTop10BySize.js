import React, { Component } from "react";
import { Box } from "@mui/material";
import MaterialReactTable from "material-react-table";

export const columns = [
  {
    header: "Category",
    accessorKey: "category",
    GroupedCell: ({ cell, row }) => (
      <Box sx={{ color: "primary.main" }}>
        <strong>{cell.getValue()} </strong>
      </Box>
    ),
  },
  {
    header: "App Identifier",
    accessorKey: "app_identifier",
    enableGrouping: false,
  },
  {
    header: "Platform",
    accessorKey: "platform",
    enableGrouping: false,
  },
  {
    header: "Size GB",
    accessorKey: "size",
    enableGrouping: false,
  },
];

export const categories = ["Games", "Music", "Helath"];

class AppsTop10BySize extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const url = "http://localhost:9090/api/v1/apps-top-10-size";
    fetch(url)
      .then((response) => {
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((json) => {
        json.forEach((item, i) =>{
          json[i].category = categories[item.category-1]
          json[i].size = Number(item.size/1024).toFixed(2)
        })
        this.setState({ data: json });
      })
      .catch((error) => {
        console.log("error: " + error);
      });
  }

  render() {
    const { data } = this.state;
    return (
      <MaterialReactTable
        columns={columns}
        data={data}
        enableGrouping
        enableStickyHeader
        enableStickyFooter
        initialState={{
          density: "compact",
          expanded: false,
          grouping: ["category"],
          sorting: [{ id: "size", desc: true }],
        }}
        muiToolbarAlertBannerChipProps={{ color: "primary" }}
        muiTableContainerProps={{ sx: { maxHeight: 700 } }}
      />
    );
  }
}

export default AppsTop10BySize;
