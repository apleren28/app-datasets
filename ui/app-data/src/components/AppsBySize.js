import React, { Component } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { getRandomRGBColor } from "../utils/randomRGB";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: true,
      text: "App by size distribution by category",
    },
  },
};

export const buckets = ["0-30MB", "61-90MB", "91-120MB", "121-150MB", "+150MB"];
export const categories = ["Games", "Music", "Helath"];

class AppsBySize extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apps: [],
    };
  }

  componentDidMount() {
    const url = "http://localhost:9090/api/v1/apps-by-size";
    fetch(url)
      .then((response) => {
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then((json) => {
        let data = [];
        for (let i = 0; i < categories.length; i++) {
          let currentCategory = { label: categories[i], data: [], backgroundColor: getRandomRGBColor() };
          let filteredByCategory = json.filter((r) => r.category == i + 1);
          for (let j = 0; j < buckets.length; j++) {
            let val = filteredByCategory.find(
              (element) => element.size_bucket == buckets[j]
            );
            if (val) {
              currentCategory.data.push(val.apps);
            }
          }
          data.push(currentCategory);
        }
        this.setState({ apps: data });
      })
      .catch((error) => {
        console.log("error: " + error);
      });
  }

  render() {
    const {apps} = this.state;
    const finalData = {
        labels: buckets,
        datasets: apps,
    };
    return  <Bar options={options} data={finalData} />;
  }
}

export default AppsBySize;
