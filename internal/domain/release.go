package domain

import "time"

type ReleasesByMonth struct {
	Releases int          `json:"releases"`
	Date     time.Time    `json:"date"`
	Category CategoryType `json:"category"`
}
