package domain

type EnvConfig struct {
	DBUser        string `env:"DB_USER"`
	DBPassword    string `env:"DB_PASSWORD"`
	DBName        string `env:"DB_NAME"`
	DBPort        string `env:"DB_PORT" defaut:"5432"`
	DBMaxOpenCoon int    `env:"DB_MAX_OPEN_CONN" defaut:"20"`
	DBMaxIdleConn int    `env:"DB_MAX_IDLE_CONN" defaut:"10"`
	Environment   string `env:"ENVIRONMENT" defaut:"dev"`
}
