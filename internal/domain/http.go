package domain

import "encoding/json"

const (
	InternalErrorCode    = "I500"
	InternalErrorMessage = "Something went wrong"
)

const (
	ContentTypeHeader = "Content-Type"
	JsonContentType   = "applciation/json"
)

type Response struct {
	Code    string
	Message string
}

type InternalError struct {
	Response
}

type BadRequest struct {
	Response
}

// GetMarshalledInternalError receives optional message to
// override the generic internal error message
func GetMarshalledInternalError(message string) []byte {
	m := InternalErrorMessage
	if message != "" {
		m = message
	}

	r, _ := json.Marshal(InternalError{
		Response: Response{
			Code:    InternalErrorCode,
			Message: m,
		}})

	return r
}
