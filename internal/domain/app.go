package domain

import "time"

type App struct {
	ID            int          `json:"id"`
	AppIdentifier string       `json:"app_identifier"`
	Category      CategoryType `json:"category"`
	Size          int          `json:"size"`
	ReleaseDate   time.Time    `json:"release_date"`
	AverageRating float64      `json:"average_rating"`
	Reviews       int          `json:"reviews"`
	Platform      string       `json:"platform"`
}

type AppSize struct {
	SizeBucket string       `json:"size_bucket"`
	Apps       int          `json:"apps"`
	Category   CategoryType `json:"category"`
}

type AppReviews struct {
	AvgReview float64      `json:"averge_review"`
	Category  CategoryType `json:"category"`
}
