package domain

type CategoryType int

const (
	Game                 CategoryType = 1
	Music                CategoryType = 2
	Health               CategoryType = 3
	CategoryNotSupported              = 100
)

func (c CategoryType) ToString() string {
	switch c {
	case 1:
		return "Game"
	case 2:
		return "Music"
	case 3:
		return "Health"
	default:
		return "Undefined"
	}
}
