package dateutils_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	dateutils "gitlab.com/apleren28/internal/utils/date"
)

func TestParseAndroidDate(t *testing.T) {
	t.Run("Good Android Playstore date format", func(t *testing.T) {
		result, err := dateutils.ParseAndroidDate("Feb 26, 2020")
		assert.Nil(t, err)
		assert.Equal(t, result.String(), "2020-02-26 00:00:00 +0000 UTC")
	})

	t.Run("Good Android Playstore date format #2", func(t *testing.T) {
		result, err := dateutils.ParseAndroidDate("Feb 7, 2020")
		assert.Nil(t, err)
		assert.Equal(t, result.String(), "2020-02-07 00:00:00 +0000 UTC")
	})

	t.Run("Good Android Playstore date format #2", func(t *testing.T) {
		result, err := dateutils.ParseAndroidDate("Feb 05, 2020")
		assert.Nil(t, err)
		assert.Equal(t, result.String(), "2020-02-05 00:00:00 +0000 UTC")
	})

	t.Run("Wrong Android Playstore date format", func(t *testing.T) {
		result, err := dateutils.ParseAndroidDate("Feb26-2020")
		assert.NotNil(t, err)
		assert.True(t, result.IsZero())
	})

	t.Run("Empty Android Playstore date format", func(t *testing.T) {
		result, err := dateutils.ParseAndroidDate("")
		assert.Nil(t, err)
		assert.True(t, result.IsZero())
	})

	t.Log("sada", time.Time{}.String())
}

func TestParseiOSDate(t *testing.T) {
	t.Run("Good iOS Appstore date format", func(t *testing.T) {
		result, err := dateutils.ParseiOSDate("2017-09-28T03:02:41Z")
		assert.Nil(t, err)
		assert.Equal(t, result.String(), "2017-09-28 03:02:41 +0000 UTC")
	})

	t.Run("Empty iOS Appstore date format", func(t *testing.T) {
		result, err := dateutils.ParseiOSDate("2017-09-28T03:02:41Z")
		assert.Nil(t, err)
		assert.True(t, result.IsZero())
	})

	t.Run("Wrong iOS Appstore date format", func(t *testing.T) {
		result, err := dateutils.ParseAndroidDate("2017-09-28 H02M02")
		assert.NotNil(t, err)
		assert.True(t, result.IsZero())
	})
}
