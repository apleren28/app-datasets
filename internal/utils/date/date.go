package dateutils

import "time"

const androidDateLayaout string = "Jan 2, 2006"

// ParseAndroidDate parses the android date format from the playstore
// into the system stadard format
func ParseAndroidDate(date string) (time.Time, error) {
	if date == "" {
		return time.Time{}, nil
	}

	return time.Parse(androidDateLayaout, date)
}

// ParseiOSDate parses the iOS date format from the appstore
// into the system stadard format
func ParseiOSDate(date string) (time.Time, error) {
	if date == "" {
		return time.Time{}, nil
	}

	return time.Parse(time.RFC3339, date)
}
