package numberutils

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	// readable units available for converstion
	Bytes string = "B"
	KB    string = "K"
	MB    string = "M"
	GB    string = "G"

	BytesUnits float64 = 1
	KBUnits    float64 = 1024
	MBUnits    float64 = 1048576
	GBUnits    float64 = 1073741824
)

var (
	conversionMap = map[string]float64{
		Bytes: BytesUnits,
		KB:    KBUnits,
		MB:    MBUnits,
		GB:    GBUnits,
	}
)

// ConvertToBytes receives a human readble number representing a digital storage units
// and returns the same value as integer represented in megabytes.
func ConvertToMegaBytes(number string) int {
	if number == "0" || number == "" {
		return 0
	}

	value := number[:len(number)-1]
	units := number[len(number)-1:]

	value = strings.ReplaceAll(value, ",", "") // remove comma separator and only keep . (dots)
	units = strings.ToUpper(units)

	// one of them is empty, or the value starts by letters
	if units == "" || value == "" {
		fmt.Println("the units or values is malformed", value)
		return 0
	}

	valueF, err := strconv.ParseFloat(value, 64)
	if err != nil {
		fmt.Println("unable to convert value into a numeric representation", value)
		return 0
	}

	unitsF, ok := conversionMap[units]
	if !ok {
		fmt.Println("units not supported yet", units)
	}

	return int((valueF * unitsF) / MBUnits)
}

// ConvertBytesToMegaBytes converts a digital size into megabytes
func ConvertBytesToMegaBytes(numInBytes int) int {
	return numInBytes / 1024 / 1024
}

// ParseStringToInt tries to parse an string number into integer
// this function does not return an error but 0 as default in such case
// if error is needed then use the go built in fn for this
func ParseStringToInt(numberValue string) int {
	if len(numberValue) == 0 {
		return 0
	}

	result, err := strconv.Atoi(numberValue)
	if err != nil {
		fmt.Println("there was an error converting int number from string", numberValue)
	}

	return result
}

// ParseStringToFloat tries to parse an string number into float64
// this function does not return an error but 0 as default in such case
// if error is needed then use the go built in fn for this
func ParseStringToFloat(numberValue string) float64 {
	if len(numberValue) == 0 {
		return 0
	}

	result, err := strconv.ParseFloat(numberValue, 64)
	if err != nil {
		fmt.Println("there was an error converting float number from string", numberValue)
	}

	return result
}
