package categoryutils

import (
	"gitlab.com/apleren28/internal/domain"
)

// ParseAndroidCategory parses android categories into system
// standard categories
func ParseAndroidCategory(category string) domain.CategoryType {
	switch category {
	case "Action", "Adventure", "Arcade", "Board", "Card", "Casino",
		"Casual", "Educational", "Music", "Puzzle", "Racing", "Role Playing",
		"Simulation", "Sports", "Strategy", "Trivia":
		return domain.Game
	case "Music & Audio":
		return domain.Music
	case "Health & Fitness":
		return domain.Health
	default:
		return domain.CategoryNotSupported
	}
}

// ParseiOSCategory parses iOS categories into system
// standard categories
func ParseiOSCategory(category string) domain.CategoryType {
	switch category {
	case "Games":
		return domain.Game
	case "Music":
		return domain.Music
	case "Health & Fitness":
		return domain.Health
	default:
		return domain.CategoryNotSupported
	}
}
