package api

import (
	"encoding/json"
	"net/http"

	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/service/reports"
)

type ReportsHandler struct {
	reportsService reports.Service
}

func NewReportsHandler(reportsService reports.Service) *ReportsHandler {
	return &ReportsHandler{reportsService}
}

func (a *ReportsHandler) ListAppSizeByCategory(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	w.Header().Add(domain.ContentTypeHeader, domain.JsonContentType)
	apps, err := a.reportsService.ListAppSizeByCategory(ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	result, err := json.Marshal(apps)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func (a *ReportsHandler) ListAppAvgRatingByCategory(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	w.Header().Add(domain.ContentTypeHeader, domain.JsonContentType)
	apps, err := a.reportsService.ListAppAvgRatingByCategory(ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	result, err := json.Marshal(apps)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func (a *ReportsHandler) ListReleasesOverTimeByCategory(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	w.Header().Add(domain.ContentTypeHeader, domain.JsonContentType)
	releases, err := a.reportsService.ListReleasesOverTimeByCategory(ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	result, err := json.Marshal(releases)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func (a *ReportsHandler) ListTop10BiggestAppsByCategory(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	w.Header().Add(domain.ContentTypeHeader, domain.JsonContentType)
	apps, err := a.reportsService.GetTop10BiggestAppsByCategory(ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	result, err := json.Marshal(apps)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(domain.GetMarshalledInternalError(""))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(result)
}
