package storage

import (
	"context"

	"gitlab.com/apleren28/internal/domain"
)

type Service interface {
	InsertApp(ctx context.Context, app domain.App) error
	IsAppTableEmpty(ctx context.Context, app domain.App) (bool, error)
	GetAppSizesByCategory(ctx context.Context) ([]domain.AppSize, error)
	GetAppAvgReviewByCategory(ctx context.Context) ([]domain.AppReviews, error)
	GetReleasesOverTimeByCategory(ctx context.Context) ([]domain.ReleasesByMonth, error)
	GetTop10BiggestApps(ctx context.Context) ([]domain.App, error)
}
