package postgres

const (
	CREATE_APP = `INSERT INTO APPS (app_identifier, category, size, release_date, average_rating, reviews, platform) 
	VALUES($1, $2, $3, $4, $5, $6, $7)`

	IS_APP_TABLE_EMPTY = `SELECT id
	FROM "APPS"
	FETCH FIRST 1 ROWS ONLY`

	APP_SIZE_DIST = `
		SELECT '0-30MB' AS app_size,
		count(SIZE) AS apps,
		category
		FROM apps
		WHERE SIZE BETWEEN 0 AND 30
		GROUP BY category union
		(SELECT '31-60MB' AS app_size, count(SIZE) AS apps, category
		FROM apps
		WHERE SIZE BETWEEN 31 AND 60
		GROUP BY category) union
		(SELECT '61-90MB' AS app_size, count(SIZE) AS apps, category
		FROM apps
		WHERE SIZE BETWEEN 61 AND 90
		GROUP BY category) union
		(SELECT '91-120MB' AS app_size, count(SIZE) AS apps, category
		FROM apps
		WHERE SIZE BETWEEN 91 AND 120
		GROUP BY category) union
		(SELECT '121-150MB' AS app_size, count(SIZE) AS apps, category
		FROM apps
		WHERE SIZE BETWEEN 121 AND 150
		GROUP BY category) union
		(SELECT '+150MB' AS app_size, count(SIZE) AS apps, category
		FROM apps
		WHERE SIZE > 150
		GROUP BY category);
	`

	APP_REVIEW_DIS = `
	SELECT SUM(reviews*average_rating)/SUM(reviews) AS avg_review,
	category
	FROM apps
	GROUP BY category;
	`

	APP_RELEASES_DIST_BY_MONTH = `
		SELECT count(id) AS releases,
		date_trunc('month', release_date) AS app_release_date,
		category
		FROM apps
		WHERE date_part('year', release_date) > 1
		GROUP BY date_trunc('month', release_date),
			category
		ORDER BY app_release_date ASC;
	`

	APP_TOP_10_BY_SIZE = `
	SELECT id,
	app_identifier,
	category,
	size,
	release_date,
	average_rating,
	reviews,
	platform
	FROM
	(SELECT a.id,
		a.app_identifier,
		a.category,
		a.size,
		a.release_date,
		a.average_rating,
		a.reviews,
		a.platform,
		ROW_NUMBER() OVER (PARTITION BY category
							ORDER BY SIZE DESC) AS row_num
	FROM apps a) AS category_partion
	WHERE row_num <= 10;
	`
)
