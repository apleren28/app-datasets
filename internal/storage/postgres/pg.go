package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/storage"

	_ "github.com/lib/pq"
)

type PGDB struct {
	db *sql.DB
}

func NewPGBD(userName string, password string, dbName string, dbPort string, maxIdleConn int, maxOpenConn int) (storage.Service, error) {
	connStr := fmt.Sprintf("host=localhost port=%s user=%s password=%s dbname=%s sslmode=disable", dbPort, userName, password, dbName)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(maxIdleConn)
	db.SetMaxOpenConns(maxOpenConn)
	db.SetConnMaxLifetime(3 * time.Minute)
	db.SetConnMaxIdleTime(5 * time.Minute)
	return &PGDB{db}, nil
}

// InsertApp inserts a new app into the DB
func (s *PGDB) InsertApp(ctx context.Context, app domain.App) error {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return err
	}

	defer c.Close()
	_, err = c.ExecContext(
		ctx,
		CREATE_APP,
		app.AppIdentifier,
		app.Category,
		app.Size,
		app.ReleaseDate,
		app.AverageRating,
		app.Reviews,
		app.Platform,
	)

	return err
}

func (s *PGDB) GetAppSizesByCategory(ctx context.Context) ([]domain.AppSize, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return nil, err
	}

	defer c.Close()
	rows, err := c.QueryContext(ctx, APP_SIZE_DIST)
	if err != nil {
		return nil, err
	}

	result := []domain.AppSize{}
	defer rows.Close()

	for rows.Next() {
		app := domain.AppSize{}
		if err := rows.Scan(&app.SizeBucket, &app.Apps, &app.Category); err != nil {
			return nil, err
		}

		result = append(result, app)
	}

	return result, nil
}

func (s *PGDB) GetAppAvgReviewByCategory(ctx context.Context) ([]domain.AppReviews, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return nil, err
	}

	defer c.Close()
	rows, err := c.QueryContext(ctx, APP_REVIEW_DIS)
	if err != nil {
		return nil, err
	}

	result := []domain.AppReviews{}
	defer rows.Close()

	for rows.Next() {
		app := domain.AppReviews{}
		if err := rows.Scan(&app.AvgReview, &app.Category); err != nil {
			return nil, err
		}

		result = append(result, app)
	}

	return result, nil
}

func (s *PGDB) GetReleasesOverTimeByCategory(ctx context.Context) ([]domain.ReleasesByMonth, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return nil, err
	}

	defer c.Close()
	rows, err := c.QueryContext(ctx, APP_RELEASES_DIST_BY_MONTH)
	if err != nil {
		return nil, err
	}

	result := []domain.ReleasesByMonth{}
	defer rows.Close()

	for rows.Next() {
		app := domain.ReleasesByMonth{}
		if err := rows.Scan(&app.Releases, &app.Date, &app.Category); err != nil {
			return nil, err
		}

		result = append(result, app)
	}

	return result, nil
}

func (s *PGDB) GetTop10BiggestApps(ctx context.Context) ([]domain.App, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return nil, err
	}

	defer c.Close()
	rows, err := c.QueryContext(ctx, APP_TOP_10_BY_SIZE)
	if err != nil {
		return nil, err
	}

	result := []domain.App{}

	defer rows.Close()
	for rows.Next() {
		app := domain.App{}
		if err := rows.Scan(
			&app.ID,
			&app.AppIdentifier,
			&app.Category,
			&app.Size,
			&app.ReleaseDate,
			&app.AverageRating,
			&app.Reviews,
			&app.Platform,
		); err != nil {
			return nil, err
		}

		result = append(result, app)
	}

	return result, nil
}

func (s *PGDB) IsAppTableEmpty(ctx context.Context, app domain.App) (bool, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return false, err
	}
	defer c.Close()

	rows, err := c.QueryContext(ctx, IS_APP_TABLE_EMPTY)
	if err != nil {
		return false, err
	}
	defer rows.Close()
	rows.Next()

	var id int
	rows.Scan(&id)
	if id != 0 {
		return false, nil
	}

	return true, err
}
