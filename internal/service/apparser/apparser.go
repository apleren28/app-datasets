package apparser

type Service interface {
	// ParseAndStoreAppData reads the app data from the source file
	// and then store the information in the final data storage
	ParseAndStoreAppData(dataLocation string) error
}
