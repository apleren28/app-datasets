package apparser

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync/atomic"
	"time"

	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/service/filereader"
	"gitlab.com/apleren28/internal/storage"
	categoryutil "gitlab.com/apleren28/internal/utils/category"
	dateutils "gitlab.com/apleren28/internal/utils/date"
	numberutils "gitlab.com/apleren28/internal/utils/numbers"
)

type IOSApp struct {
	fileReader    filereader.Service
	storage       storage.Service
	notifier      chan int64
	totalIngested int64
}

func NewIOSApp(fileReader filereader.Service, storage storage.Service, notifier chan int64) Service {
	return &IOSApp{fileReader, storage, notifier, 0}
}

// ParseAndStoreAppData reads the iOS app data from the source file
// and then store the information in the final data storage
func (a *IOSApp) ParseAndStoreAppData(dataLocation string) error {
	if dataLocation == "" {
		return ErrInvalidFileLocation
	}

	fmt.Println("started reading iOS dataset")
	data := make(chan []string)
	errChan := make(chan error)
	// this channel limits the maximun number of go routines (green threads) are
	// created to call the DB, once the buffer is reached the main thread is blocked until
	// the max number is freed at least by 1
	maxDBCalls := make(chan bool, 10)
	defer close(data)
	defer close(errChan)
	defer close(maxDBCalls)

	ctx, cancel := context.WithCancel(context.Background())
	// read file async and get one record at the time for processing
	// so the process can be boosted
	go a.fileReader.ReadFile(dataLocation, data, errChan, cancel)
	var valid, invalid int64 = 0, 0

	for {
		select {
		case dataRead := <-data:
			app, err := a.ParseCSVIntoModel(dataRead)
			if errors.Is(err, ErrAppCategoryNotSupported) {
				invalid++
				continue
			}

			if err != nil {
				return err
			}

			maxDBCalls <- true
			go func(valid *int64, maxDBCalls chan bool, app domain.App) {
				ctx, dbCancel := context.WithTimeout(context.Background(), time.Second*5)
				defer dbCancel()
				defer func() { <-maxDBCalls }()
				if err := a.storage.InsertApp(ctx, app); err != nil {
					fmt.Println("error inserting the iOS app", app.AppIdentifier, err)
				} else {
					atomic.AddInt64(valid, 1)
				}
			}(&valid, maxDBCalls, app)

			ingested := valid + invalid - a.totalIngested
			// notify every 50K records to the caller
			if ingested > 50_000 {
				a.totalIngested += ingested
				a.notifier <- ingested
			}

		case err := <-errChan:
			log.Fatal("error processing iOS dataset", err)
		case <-ctx.Done():
			fmt.Println("just finished")
			//blocks until all DB calls are finished
			for len(maxDBCalls) > 0 {
			}
			fmt.Println("ingested", valid, "iOS apps within the categories of interest and", invalid, "out of interest")
			return nil
		}
	}
}

// ParseCSVIntoModel parses the expected CSV data into the systems' app
// model
func (a *IOSApp) ParseCSVIntoModel(row []string) (domain.App, error) {
	dateFormated, err := dateutils.ParseiOSDate(row[7])
	if err != nil {
		return domain.App{}, err
	}

	categoryFormated := categoryutil.ParseiOSCategory(row[3])
	if categoryFormated == domain.CategoryNotSupported {
		return domain.App{}, ErrAppCategoryNotSupported
	}

	appSizeBytes := numberutils.ParseStringToInt(row[5])

	return domain.App{
		AppIdentifier: row[0],
		Category:      categoryFormated,
		Size:          numberutils.ConvertBytesToMegaBytes(appSizeBytes),
		ReleaseDate:   dateFormated,
		AverageRating: numberutils.ParseStringToFloat(row[17]),
		Reviews:       numberutils.ParseStringToInt(row[18]),
		Platform:      "iOS",
	}, nil
}
