package apparser

import "errors"

var (
	ErrAppCategoryNotSupported = errors.New("this app category is not supported at the moment")
	ErrInvalidFileLocation     = errors.New("invalida file location")
)
