package filereader

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
)

var (
	ErrFileNotFound = errors.New("file not found or could not be opened")
	ErrReadingFile  = errors.New("error reading CSV file or is empty")
)

type CSVReader struct {
	skipHeader bool
}

func NewCSVFileReader(skipHeader bool) Service {
	return &CSVReader{skipHeader}
}

func (r *CSVReader) ReadFile(filePath string, data chan []string, errChan chan error, cancel context.CancelFunc) {
	f, err := os.Open(filePath)
	if err != nil {
		errChan <- ErrFileNotFound
		return
	}

	defer f.Close()

	reader := csv.NewReader(f)
	if r.skipHeader {
		_, err = reader.Read()
		if err != nil {
			errChan <- err
			return
		}
	}

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println("error reading file")
			break
		}

		data <- record
	}

	cancel()
}
