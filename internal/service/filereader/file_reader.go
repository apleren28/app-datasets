package filereader

import "context"

type Service interface {
	ReadFile(filePath string, data chan []string, errChan chan error, cancel context.CancelFunc)
}
