package reports

import (
	"context"

	"gitlab.com/apleren28/internal/domain"
)

type Service interface {
	ListAppSizeByCategory(ctx context.Context) ([]domain.AppSize, error)
	ListAppAvgRatingByCategory(ctx context.Context) ([]domain.AppReviews, error)
	ListReleasesOverTimeByCategory(ctx context.Context) ([]domain.ReleasesByMonth, error)
	GetTop10BiggestAppsByCategory(ctx context.Context) ([]domain.App, error)
}
