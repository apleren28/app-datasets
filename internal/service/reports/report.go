package reports

import (
	"context"

	"gitlab.com/apleren28/internal/domain"
	"gitlab.com/apleren28/internal/storage"
)

type Reports struct {
	storage storage.Service
}

func NewReportService(storage storage.Service) Service {
	return &Reports{storage}
}

func (r *Reports) ListAppSizeByCategory(ctx context.Context) ([]domain.AppSize, error) {
	return r.storage.GetAppSizesByCategory(ctx)
}
func (r *Reports) ListAppAvgRatingByCategory(ctx context.Context) ([]domain.AppReviews, error) {
	return r.storage.GetAppAvgReviewByCategory(ctx)
}
func (r *Reports) ListReleasesOverTimeByCategory(ctx context.Context) ([]domain.ReleasesByMonth, error) {
	return r.storage.GetReleasesOverTimeByCategory(ctx)
}
func (r *Reports) GetTop10BiggestAppsByCategory(ctx context.Context) ([]domain.App, error) {
	return r.storage.GetTop10BiggestApps(ctx)
}
