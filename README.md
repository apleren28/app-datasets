# DS-MOBILE-APP

This application loads datasets representing a subset of the apps in the iOS App Store and Google Play Store

- Plot the distribution of app sizes across our three categories.
- Compute the average rating across all ratings for each of the three categories. 
- Plot the number of new releases in each category over time by month.
- List the 10 biggest apps by size in each category by year.

## Local setup

### requirements
- Install golang [here](https://go.dev/doc/install) or `brew install go`
- Install Docker [here](https://docs.docker.com/get-docker/) or `brew install docker` 
- Install `go-task` [here](https://taskfile.dev/installation/) to run the predfined tasks
- Install nodeJS v18 [here](https://nodejs.org/en/download/) or `brew install node@18`

## Applications

### Ingestor

Reads the mobile datasets and insert the corresponding records into the defined Postgres DB. Located [here](./cmd/ingester/)

## Migrations

Helper program that runs up and down migrations to facilitate integration tests. Located [here](./cmd/migrations/)

## Service

Spins up a http server that exposes some APIS for the front end. Located [here](./cmd/service/)


## Web app

Single Page Application with the requrided charts to visualize the data.  Located [here](./ui/app-data/)


## Run integration tests

if you have install all required applications you can run the integration tests by running a single task command.

1. copy the file `.env.dist` into `.env.dev` and update the values `DB_USER`, `DB_PASSWORD` and `DB_NAME` as per your convenience.
2. run `task run-integration-test` in the root of the project. This will unzip the testing datasets, starts up a DB dedicated for the integration tests and then runs the ingestor to populate the DB to finally run the integration tests and then clean up the DB.

## Start up the application(s)

### start the backend

1. copy the file `.env.dist` into `.env` and update the values `DB_USER`, `DB_PASSWORD` and `DB_NAME` as per your convenience, update `ENVIRONMENT` to `prod` and `DB_PORT` to `5432`.
2. run `task ingest-prod-data` to populate the DB with the iOS and Android datasets. This might take some time depending on the host OS. usually 15mins.
3. run `task run-http-server` this task spins up a http server in the port `9090` with the APIS created for the frontend. For more details check [here](.api/) a postman collection to play around.


### start the frontend

1. run `task run-web-app` this might take some minutes while the node modules are installed, once is ready a new server is created in the port `3000`. Once ready you can see the charts loaded if the backend is well setup.


## Notes

- The queries used to populate the charts in the frontend can be found [here](./internal/storage/postgres/queries.go)
- The project is essentially composed by 3 parts, 1. the ingestor that populates the DB, the service that exposes and HTTP service layer with different APIs and the web app that send requests to the APIs to render the charts.
- If for some reason you are unable to run the `task` then go [here](Taskfile.yaml) and inspect the sequential steps done in order to run the multiple phases.
- In `./data/{dev|prod}` lies the datasets in compressed tar files.
