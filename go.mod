module gitlab.com/apleren28

go 1.17

require (
	github.com/lib/pq v1.10.7
	github.com/stretchr/testify v1.8.1
)

require (
	github.com/caarlos0/env/v6 v6.10.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	github.com/go-chi/cors v1.2.1 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/pressly/goose/v3 v3.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
